#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
import sys

from playhouse.db_url import connect

from server.models import (
    DATABASE_PROXY, init_proxy, Meuble
)

from server.routes.platforms import get_full_ban_from_api_batch

## Ban update
# If you want to use BAN update (https://adresse.data.gouv.fr/api-doc/adresse), you need to create a current_ban_index.txt file containing the database id on which the script will begin.

# Nombre de meublés envoyés simultanément à la BAN
batch = 1000


def init_db():
    """
    Populate the database
    """
    init_proxy()
    DATABASE_PROXY.connect()
    # DATABASE_PROXY.create_tables(Meuble)
    if not DATABASE_PROXY.is_closed():
        DATABASE_PROXY.close()


def connect_db():
    """
    Connect to the database. Bottle hook to handle it at each request.
    """
    DATABASE_PROXY.connect(reuse_if_open=True)


def close_db():
    """
    Close the database. Bottle hook to handle it at each request.
    """
    if not DATABASE_PROXY.is_closed():
        DATABASE_PROXY.close()


def update_meubles(meubles):
    # Prepares data for querying public database
    ad_libelle_cp_list = [
        {
            'batch_id': batch_id,
            'ad_libelle': meuble.ad_libelle,
            'ad_cp': meuble.ad_cp
        }
        for batch_id, meuble in enumerate(meubles)
    ]
    geocoded_adresses = get_full_ban_from_api_batch(ad_libelle_cp_list)
    for batch_id, meuble in enumerate(meubles):
        try:
            if(geocoded_adresses[str(batch_id)]['ad_ban'] is None):
                continue
            meuble.ad_normalise = geocoded_adresses[str(batch_id)]['ad_normalise']
            meuble.ad_latitude_normalise = geocoded_adresses[str(
                batch_id)]['ad_latitude_normalise']
            meuble.ad_longitude_normalise = geocoded_adresses[str(
                batch_id)]['ad_longitude_normalise']
            meuble.score = geocoded_adresses[str(batch_id)]['score']
        except Exception as e:
            sys.stderr.write(
                "Error matching geocoded_adresses fields to ad_libelle\n" + 'error message: ' + str(e) + "\n")
    return meubles


init_db()

# Get current BAN index (i.e l'identifiant auquel on s'est arrêté)
f = open('current_ban_index.txt', "r+")
f.seek(0)
buffer = f.read()
API_BAN_INDEX = int(buffer) if buffer else 0
f.close()

# print("Meuble data has been upgraded with BAN up to id:", API_BAN_INDEX)

# Get last DB id
connect_db()
DB_INDEX = Meuble.select(Meuble.id).order_by(Meuble.id.desc()).get().id
close_db()

error = False
idx = API_BAN_INDEX
if API_BAN_INDEX >= DB_INDEX:
    sys.stderr.write("Current BAN index above DB_INDEX, check BAN file")
while idx < DB_INDEX:
    with DATABASE_PROXY.atomic() as transaction:
        try:
            meubles = Meuble.select().where(Meuble.id > idx).order_by(Meuble.id).limit(batch)
            meubles = update_meubles(meubles)
            for meuble in meubles:
                meuble.save()
            idx = meubles[-1].id
        except Exception as e:
            sys.stderr.write(
                "Error while trying to enhance data\n" + str(e) + "\n")
            transaction.rollback()
            error = True
            break
close_db()

# Update the BAN index
f = open('current_ban_index.txt', "w")
f.write(str(idx))
f.close()

if(error):
    sys.exit(1)
