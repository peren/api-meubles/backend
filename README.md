<!--
MIT License

Copyright (c) 2022 PEReN

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.-->

API Meublés de tourisme - backend
=================================

This project experiment ended in october 2022. The team has begun working on ways to implement the solution on a larger scale.
In the mean time an archive of the project is available [here](https://meubles.peren.fr/).

You can contact us by sending an e-mail to contact.peren@finances.gouv.fr.

## Set up

```
pip install -r requirements.txt
```


## Dev

Use environment variables `HOST` and `PORT` to change development web server
configuration. Use `DEBUG=1` to output debug log (especially all SQL
queries).

Run `python -m server run` to start the webserver.


## Prod

Use environment variable `DATABASE` to specify a database connection URI. For
example,

```
env = DATABASE=postgresql://postgres:my_password@localhost:5432/my_database
```


## Tools

Use `python -m server generate_token` to get an API key.

* `python -m server generate_token meubles owner_name` for a platform API key.
* `python -m server generate_token meubles owner_name timestamp` for a platform API key and an expiration timestamp.

## License

See LICENSE.md
