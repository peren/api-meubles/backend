#!/usr/bin/env python
# coding: utf-8
#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Test platforms API routes
"""
import copy
import csv
import io
import os
import tempfile
import unittest

import arrow
import bottle
import webtest

from server import APP, generate_api_key, init_db
from server.models import Meuble, PayloadDump
from distutils.util import strtobool

bottle.debug(True)
CURRENT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))


class TestPostAccomodation(unittest.TestCase):
    def setUp(self):
        self.db_tmp = tempfile.NamedTemporaryFile(suffix='.db')
        os.environ['DATABASE'] = 'sqlite:///' + self.db_tmp.name
        init_db()
        self.APP = webtest.TestApp(APP)
        self.test_api_key = generate_api_key('meubles', 'test')

    def tearDown(self):
        self.db_tmp.close()

    def test_no_API_key_or_bad_API_key(self):
        """
        No API key
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_batch.csv'
        )
        for msg, post_args in [
            ("no api key", {}),
            ("bad api key", {"headers": {'X-API-KEY': 'DOES_NOT_EXIST'}})
        ]:
            with self.subTest(msg=msg):
                with open(FILENAME) as fh:
                    response = self.APP.post(
                        '/api/v1/meubles', fh.read(),
                        expect_errors=True,
                        **post_args
                    )
                self.assertEqual(403, response.status_int)
                self.assertEqual(Meuble.select().count(), 0)
                self.assertEqual(PayloadDump.select().count(), 0)

    def test_bad_API_key_scope(self):
        """
        Invalid scope for API key
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_batch.csv'
        )
        with open(FILENAME) as fh:
            test_api_key = generate_api_key('invalid', 'test')
            response = self.APP.post(
                '/api/v1/meubles', fh.read(),
                headers={'X-API-KEY': test_api_key},
                expect_errors=True
            )
        self.assertEqual(403, response.status_int)
        self.assertEqual(Meuble.select().count(), 0)
        self.assertEqual(PayloadDump.select().count(), 0)

    def test_complete(self):
        """
        Complete data
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_meuble_complet_unique.csv'
        )
        with open(FILENAME) as fh:
            file = fh.read()
            response = self.APP.post(
                '/api/v1/meubles', file,
                headers={'X-API-KEY': self.test_api_key}
            )
        self.assertEqual(200, response.status_int)

        with open(FILENAME) as fh:
            meuble_list = list(Meuble.select())
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            for meuble_line, meuble_CSV_line in zip(meuble_list, meuble_list_CSV):
                self.assertIn(meuble_CSV_line['id_meuble'], response.json['detail']['status'].keys())
                for field, value in meuble_CSV_line.items():
                    if 'date_' in field:  # Proxy for date_debut and date_fin
                        value = arrow.get(value, 'DD/MM/YYYY').date()
                    elif field == 'est_residence_principale':
                        value = strtobool(value)
                    elif field.startswith('nb_'):  # Possible update with multiple night numbers
                        value = int(value)
                    elif field == 'ad_num_app' or field == 'ad_etage':
                        value = int(value) if value else None
                    elif field == 'ad_geo':
                        if (value != ''):
                            value1, value2 = map(float, value.split(','))
                        else:
                            value1, value2 = None, None
                        self.assertEqual(
                            getattr(meuble_line, 'ad_latitude'),
                            value1,
                            msg=field
                        )
                        self.assertEqual(
                            getattr(meuble_line, 'ad_longitude'),
                            value2,
                            msg=field
                        )
                        continue
                    self.assertEqual(
                        getattr(meuble_line, field),
                        value,
                        msg=field
                    )

            self.assertEqual(len(meuble_list), len(meuble_list_CSV))
            self.assertEqual(PayloadDump.select().count(), 1)

    def test_minimal_tous_champs(self):
        """
        Minimal example
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_meuble_minimal_unique.csv'
        )
        with open(FILENAME) as fh:
            file = fh.read()
            response = self.APP.post(
                '/api/v1/meubles', file,
                headers={'X-API-KEY': self.test_api_key}
            )
        self.assertEqual(200, response.status_int)

        with open(FILENAME) as fh:
            meuble_list = list(Meuble.select())
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            for meuble_line, meuble_CSV_line in zip(meuble_list, meuble_list_CSV):
                self.assertIn(meuble_CSV_line['id_meuble'], response.json['detail']['status'].keys())
                for field, value in meuble_CSV_line.items():
                    if 'date_' in field:  # Proxy for date_debut and date_fin
                        value = arrow.get(value, 'DD/MM/YYYY').date()
                    elif field == 'est_residence_principale':
                        value = strtobool(value)
                    elif field.startswith('nb_'):  # Possible update with multiple night numbers
                        value = int(value)
                    elif field == 'ad_num_app' or field == 'ad_etage':
                        value = int(value) if value else None
                    elif field == 'ad_ban':  # triche car l'id_ban est update via la BAN
                        value = getattr(meuble_line, field)
                    elif field == 'ad_geo':
                        if (value != ''):
                            value1, value2 = map(float, value.split(','))
                        else:
                            value1, value2 = None, None
                        self.assertEqual(
                            getattr(meuble_line, 'ad_latitude'),
                            value1,
                            msg=field
                        )
                        self.assertEqual(
                            getattr(meuble_line, 'ad_longitude'),
                            value2,
                            msg=field
                        )
                        continue
                    self.assertEqual(
                        getattr(meuble_line, field),
                        value,
                        msg=field
                    )

            self.assertEqual(len(meuble_list), len(meuble_list_CSV))
            self.assertEqual(PayloadDump.select().count(), 1)

    def test_minimal_champs_vides(self):
        """
        Minimal example
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_meuble_minimal_unique_vide.csv'
        )
        with open(FILENAME) as fh:
            file = fh.read()
            response = self.APP.post(
                '/api/v1/meubles', file,
                headers={'X-API-KEY': self.test_api_key}
            )
        self.assertEqual(200, response.status_int)

        with open(FILENAME) as fh:
            meuble_list = list(Meuble.select())
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            for meuble_line, meuble_CSV_line in zip(meuble_list, meuble_list_CSV):
                self.assertIn(meuble_CSV_line['id_meuble'], response.json['detail']['status'].keys())
                for field, value in meuble_CSV_line.items():
                    if 'date_' in field:  # Proxy for date_debut and date_fin
                        value = arrow.get(value, 'DD/MM/YYYY').date()
                    elif field == 'est_residence_principale':
                        value = strtobool(value)
                    elif field.startswith('nb_'):  # Possible update with multiple night numbers
                        value = int(value)
                    elif field == 'ad_num_app' or field == 'ad_etage':
                        value = int(value) if value else None
                    elif field == 'ad_ban':  # triche car l'id_ban est update via la BAN
                        value = getattr(meuble_line, field)
                    self.assertEqual(
                        getattr(meuble_line, field),
                        value,
                        msg=field
                    )

            self.assertEqual(len(meuble_list), len(meuble_list_CSV))
            self.assertEqual(PayloadDump.select().count(), 1)

    def test_batch_CSV(self):
        """
        Example with the Airbnb provided CSV
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_batch.csv'
        )
        with open(FILENAME) as fh:
            file = fh.read()
            response = self.APP.post(
                '/api/v1/meubles', file,
                headers={'X-API-KEY': self.test_api_key}
            )
        self.assertEqual(200, response.status_int)

        with open(FILENAME) as fh:
            meuble_list = list(Meuble.select())
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            for meuble_line, meuble_CSV_line in zip(meuble_list, meuble_list_CSV):
                self.assertIn(meuble_CSV_line['id_meuble'], response.json['detail']['status'].keys())
                for field, value in meuble_CSV_line.items():
                    if 'date_' in field:  # Proxy for date_debut and date_fin
                        value = arrow.get(value, 'DD/MM/YYYY').date()
                    elif field == 'est_residence_principale':
                        value = strtobool(value)
                    elif field.startswith('nb_'):  # Possible update with multiple night numbers
                        value = int(value)
                    elif field == 'ad_num_app' or field == 'ad_etage':
                        value = int(value) if value else None
                    elif field == 'ad_ban':  # triche car l'id_ban est update via la BAN
                        value = getattr(meuble_line, field)
                    elif field == 'ad_geo':
                        if (value != ''):
                            value1, value2 = map(float, value.split(','))
                        else:
                            value1, value2 = None, None
                        self.assertEqual(
                            getattr(meuble_line, 'ad_latitude'),
                            value1,
                            msg=field
                        )
                        self.assertEqual(
                            getattr(meuble_line, 'ad_longitude'),
                            value2,
                            msg=field
                        )
                        continue
                    self.assertEqual(
                        getattr(meuble_line, field),
                        value,
                        msg=field
                    )

            self.assertEqual(len(meuble_list), len(meuble_list_CSV))
            self.assertEqual(PayloadDump.select().count(), 1)

    def test_batch_long_CSV(self):
        """
        Example with the Airbnb provided CSV (long version)
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_batch_long.csv'
        )
        with open(FILENAME) as fh:
            file = fh.read()
            response = self.APP.post(
                '/api/v1/meubles', file,
                headers={'X-API-KEY': self.test_api_key}
            )
        self.assertEqual(200, response.status_int)

        with open(FILENAME) as fh:
            meuble_list = list(Meuble.select())
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            for meuble_line, meuble_CSV_line in zip(meuble_list, meuble_list_CSV):
                self.assertIn(meuble_CSV_line['id_meuble'], response.json['detail']['status'].keys())
                for field, value in meuble_CSV_line.items():
                    if 'date_' in field:  # Proxy for date_debut and date_fin
                        value = arrow.get(value, 'DD/MM/YYYY').date()
                    elif field == 'est_residence_principale':
                        value = strtobool(value)
                    elif field.startswith('nb_'):  # Possible update with multiple night numbers
                        value = int(value)
                    elif field == 'ad_num_app' or field == 'ad_etage':
                        value = int(value) if value else None
                    elif field == 'ad_ban':  # triche car l'id_ban est update via la BAN
                        value = getattr(meuble_line, field)
                    elif field == 'ad_geo':
                        if (value != ''):
                            value1, value2 = map(float, value.split(','))
                        else:
                            value1, value2 = None, None
                        self.assertEqual(
                            getattr(meuble_line, 'ad_latitude'),
                            value1,
                            msg=field
                        )
                        self.assertEqual(
                            getattr(meuble_line, 'ad_longitude'),
                            value2,
                            msg=field
                        )
                        continue
                    self.assertEqual(
                        getattr(meuble_line, field),
                        value,
                        msg=field
                    )

            self.assertEqual(len(meuble_list), len(meuble_list_CSV))
            self.assertEqual(PayloadDump.select().count(), 1)

    def test_batch_should_error_on_missing_field(self):
        """
        Example with the Airbnb provided CSV with missing field for every non mandatory field
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_meuble_complet_unique.csv'
        )
        MANDATORY_FIELDS = [
            'id_meuble', 'ad_libelle', 'ad_cp', 'ad_ville', 'est_residence_principale',
            'nb_nuits', 'date_debut', 'date_fin', 'nom_loueur'
        ]
        for field in MANDATORY_FIELDS:
            with open(FILENAME) as fh:
                meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
                with io.StringIO() as si:
                    writer = csv.DictWriter(
                        si,
                        fieldnames=[x for x in meuble_list_CSV[0].keys() if x != field],
                        delimiter=';'
                    )
                    writer.writeheader()
                    for item in meuble_list_CSV:
                        del item[field]
                        writer.writerow(item)
                    response = self.APP.post(
                        '/api/v1/meubles', si.getvalue(),
                        headers={'X-API-KEY': self.test_api_key},
                        expect_errors=True
                    )
            self.assertEqual(400, response.status_int)

        meuble_list = list(Meuble.select())
        self.assertEqual(len(meuble_list), 0)
        self.assertEqual(PayloadDump.select().count(), len(MANDATORY_FIELDS))

    def test_batch_should_error_on_too_many_fields(self):
        """
        Example with the Airbnb provided CSV with additional unexisting field
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_meuble_complet_unique.csv'
        )

        with open(FILENAME) as fh:
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))
            with io.StringIO() as si:
                writer = csv.DictWriter(
                    si,
                    fieldnames=[x for x in meuble_list_CSV[0].keys()] + ['incorrect_field'],
                    delimiter=';'
                )
                writer.writeheader()
                for item in meuble_list_CSV:
                    writer.writerow(item)
                response = self.APP.post(
                    '/api/v1/meubles', si.getvalue(),
                    headers={'X-API-KEY': self.test_api_key},
                    expect_errors=True
                )
        self.assertEqual(400, response.status_int)

        meuble_list = list(Meuble.select())
        self.assertEqual(len(meuble_list), 0)
        self.assertEqual(PayloadDump.select().count(), 1)

    def test_batch_should_error_on_invalid_values(self):
        """
        There should be an error when a value is invalid
        """
        FILENAME = os.path.join(
            CURRENT_DIRECTORY,
            'test_files/test_batch.csv'
        )

        with open(FILENAME) as fh:
            meuble_list_CSV = list(csv.DictReader(fh, delimiter=';'))

        for column, value in [
            ('ad_cp', 'AAAAA'),
            ('ad_insee', 'AAAAA'),
            ('ad_geo', 'AAAAA'),
            ('date_debut', 'AAAAA'),
            ('date_fin', 'AAAAA')] + [
            (column, value)
            for column in ['nb_nuits', 'ad_etage', 'ad_num_app']
            for value in ['AAAAA', -1]
        ]:
            with self.subTest(msg=f"{column}: {value}"):
                # Invalid INSEE code
                with io.StringIO() as si:
                    testBody = copy.deepcopy(meuble_list_CSV)
                    writer = csv.DictWriter(si, fieldnames=list(set(list(testBody[0].keys()) + [column])))
                    writer.writeheader()
                    for item in meuble_list_CSV:
                        item[column] = value
                        writer.writerow(item)
                    response = self.APP.post(
                        '/api/v1/meubles', si.getvalue(),
                        headers={'X-API-KEY': self.test_api_key},
                        expect_errors=True
                    )
                    self.assertEqual(400, response.status_int)

        # Nothing added in db
        self.assertEqual(Meuble.select().count(), 0)
        self.assertEqual(PayloadDump.select().count(), 11)
