#!/usr/bin/env python
# coding: utf-8
#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Code related to the initialization of the server (Database, keys, etc)
"""
import bottle
import functools
import json
import secrets


from datetime import datetime
from server.jsonapi import DateAwareJSONEncoder
from server.models import (
    DATABASE_PROXY, init_proxy,
    APIKey, Meuble, PayloadDump
)
from server.routes import platforms


def generate_api_key(scope, owner, expires=None):
    """
    Generate a new API key for the given scope, store it in database and return
    it.
    """
    api_key = secrets.token_hex(16)
    try:
        if expires:
            expires = datetime.fromtimestamp(int(expires))
    except Exception:
        expires = None
        print('Error handling apikey expires argument.')
        print('Value changed to None.')
    token = APIKey(token=api_key, scope=scope, owner=owner, expires_on=expires)
    token.save()
    return api_key


def init_db():
    """
    Populate the database
    """
    init_proxy()
    DATABASE_PROXY.connect()
    DATABASE_PROXY.create_tables(
        [APIKey, Meuble, PayloadDump]
    )
    if not DATABASE_PROXY.is_closed():
        DATABASE_PROXY.close()


APP = bottle.Bottle()

# Platforms API
APP.route('/api/v1/meubles', ['POST', 'OPTIONS'],
          platforms.post_batch_accomodations)


# Use DateAwareJSONEncoder to dump JSON strings
# From http://stackoverflow.com/questions/21282040/bottle-framework-how-to-return-datetime-in-json-response#comment55718456_21282666. # noqa: E501
APP.install(
    bottle.JSONPlugin(
        json_dumps=functools.partial(json.dumps, cls=DateAwareJSONEncoder)
    )
)
init_db()

# Handled server-side
# Could be used in relation with other tools.
# @APP.hook('after_request')
# def enable_cors():
#     """
#     Add CORS headers at each request.
#     """
#     # headers list should have plain str type.
#     bottle.response.headers[str('Access-Control-Allow-Origin')] = str('*')
#     # bottle.response.headers[str('Access-Control-Allow-Origin')] = str('http://localhost:8080')
#     bottle.response.headers[str('Access-Control-Allow-Methods')] = str(
#         'PUT, GET, POST, DELETE, OPTIONS, PATCH'
#     )
#     bottle.response.headers[str('Access-Control-Allow-Headers')] = str(
#         'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, '
#         'Authorization, X-API-KEY'
#     )


@APP.hook('before_request')
def _connect_db():
    """
    Connect to the database. Bottle hook to handle it at each request.
    """
    DATABASE_PROXY.connect(reuse_if_open=True)


@APP.hook('after_request')
def _close_db():
    """
    Close the database. Bottle hook to handle it at each request.
    """
    if not DATABASE_PROXY.is_closed():
        DATABASE_PROXY.close()
