#!/usr/bin/env python
# coding: utf-8
#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Routes definitions for the platforms side API
"""
import csv
import io
import re
import sys
import datetime

from distutils.util import strtobool

import bottle
import requests

from server import jsonapi
from server.auth import check_auth, AuthenticationError
from server.models import (
    DATABASE_PROXY, Meuble, PayloadDump
)
from server.tools import is_url
from server.tools import multireplace


ADDR_BAN_RE = re.compile(r'^\d{5}_[a-z0-9]\d{3}_\d{5}')
CODE_POSTAL_RE = re.compile(r'^\d{5}$')
CODE_INSEE_RE = re.compile(r'^\d{5}$')
# GEOLOCATION_RE = re.compile(
#     r'^((\-?|\+?)?\d+(\.\d+)?),\s*((\-?|\+?)?\d+(\.\d+)?)$'
# )  # source https://ihateregex.io/expr/lat-long/#


class InvalidPayloadError(Exception):
    """
    Error regarding data format
    """
    pass


def get_full_ban_from_api_batch(ad_libelle_cp_list):
    """
    Get the full address data and BAN id from the https://adresse.data.gouv.fr/api-doc/adresse
    geocoder.
    """
    search_csv = ['ad_libelle,ad_cp,batch_id']
    for item in ad_libelle_cp_list:
        search_csv.append('%s,%s,%s' %
                          (multireplace(item['ad_libelle'], {',': ' ', '"': ''}), item['ad_cp'], item['batch_id']))
    files = {
        'data': ('search.csv', ('\n'.join(search_csv)).encode('utf-8')),
    }
    # Fields result_name and result_citycode could be usefull and asked to be returned
    # Not used at the moment
    data = {
        'columns': 'ad_libelle',
        'postcode': 'ad_cp',
        'result_columns': [
            'result_type', 'result_id', 'result_label',
            'latitude', 'longitude', 'result_score'
        ],
    }
    try:
        response = requests.post(
            'https://api-adresse.data.gouv.fr/search/csv/',
            files=files,
            data=data
        ).text
        response = csv.DictReader(
            io.StringIO(response),
            delimiter=','
        )
    except Exception as e:
        sys.stderr.write("Error querying BAN API\n"
                         + 'error message: ' + str(e) + "\n")
        return {
            item['batch_id']: {
                'ad_ban': None,
                'ad_normalise': None,
                'ad_latitude_normalise': None,
                'ad_longitude_normalise': None,
                'score': None
            }
            for item in ad_libelle_cp_list
        }

    geocoded = {}
    for item in response:
        try:
            if item['result_type'] != 'housenumber':
                raise
        except Exception:
            geocoded[item['batch_id']] = {
                'ad_ban': None,
                'ad_normalise': None,
                'ad_latitude_normalise': None,
                'ad_longitude_normalise': None,
                'score': None
            }
            continue
        geocoded[item['batch_id']] = {
            'ad_ban': item['result_id'],
            'ad_normalise': item['result_label'],
            'ad_latitude_normalise': float(item['latitude']),
            'ad_longitude_normalise': float(item['longitude']),
            'score': float(item['result_score'])
        }
    return geocoded


def get_accomodation_dict(payload, token, fill_ban):
    """
    Checks data before inserting to base
    """
    # Validate some fields, the rest is done as DB constraints
    REQUIRED_FIELDS = [
        'id_meuble', 'ad_libelle', 'ad_cp', 'ad_ville', 'est_residence_principale',
        'nb_nuits', 'date_debut', 'date_fin', 'nom_loueur'
    ]
    for field in REQUIRED_FIELDS:
        # est_residence_principale is int type
        if payload.get(field) is None or (not isinstance(payload[field], int) and not
                                          payload[field]):
            raise InvalidPayloadError('Invalid. Missing "%s" field.' % field)

    if (
            payload.get('id_meuble') is not None
            and len(payload['id_meuble']) > 255
    ):
        raise InvalidPayloadError('id_meuble is too long')

    if (
            payload.get('ad_libelle') is not None
            and len(payload['ad_libelle']) > 255
    ):
        raise InvalidPayloadError('ad_libelle is too long')
    # Check postal code and insee format
    if (
            payload.get('ad_cp')
            and not CODE_POSTAL_RE.match(payload.get('ad_cp', ''))
    ):
        raise InvalidPayloadError('ad_cp format is wrong')

    if (
            payload.get('ad_insee')
            and not CODE_INSEE_RE.match(payload.get('ad_insee', ''))

    ):
        raise InvalidPayloadError('ad_insee format is wrong')

    if (
            payload.get('ad_ban')
            and not ADDR_BAN_RE.match(payload.get('ad_ban', ''))

    ):
        raise InvalidPayloadError('Invalid "ad_ban" field. Format is wrong.')

    # Check geolocation (latitude,longitude) format
    if (
            payload.get('ad_latitude')
            and not -90 <= payload.get('ad_latitude', 999) <= 90
    ):
        raise InvalidPayloadError('ad_geo not in valid range')
    if (
            payload.get('ad_longitude')
            and not -180 <= payload.get('ad_longitude', 999) <= 180
    ):
        raise InvalidPayloadError('ad_geo not in valid range')

    if (
            payload.get('ad_lot') is not None
            and len(payload['ad_lot']) > 255
    ):
        raise InvalidPayloadError('ad_lot is too long')

    if (
            payload.get('ad_bat') is not None
            and len(payload['ad_bat']) > 255
    ):
        raise InvalidPayloadError('ad_bat is too long')

    if (
            payload.get('ad_esc') is not None
            and len(payload['ad_esc']) > 255
    ):
        raise InvalidPayloadError('ad_esc is too long')

    if (
            payload.get('ad_etage')
            and payload.get('ad_etage', -1) < 0

    ):
        raise InvalidPayloadError('ad_etage is negative')
    if (
            payload.get('ad_num_app')
            and payload.get('ad_num_app', -1) <= 0

    ):
        raise InvalidPayloadError('ad_num_app is negative')

    # Temporary
    # Registration numbers must be 13 characters according to law
    # Companies are increasingly adopting this recent legislation
    # For now allow up to 3 houses on one page, example 330630004032A/3306300040010/3306300040014
    # Strict testing would only allow one registration ( == 13 characters)
    if (
            'id_num' in payload
            and payload['id_num']
            and len(payload['id_num']) > 42
    ):
        raise InvalidPayloadError('id_num is too long')

    if (
            payload.get('nom_loueur') is not None
            and len(payload['nom_loueur']) > 255
    ):
        raise InvalidPayloadError('nom_loueur')

    if not is_url(payload.get('url', '')):
        raise InvalidPayloadError('Invalid "url" field.')

    try:
        date_debut = datetime.datetime.strptime(payload.get('date_debut'), '%d/%m/%Y')
    except Exception:
        raise InvalidPayloadError('date_debut format is wrong')

    try:
        date_fin = datetime.datetime.strptime(payload.get('date_fin'), '%d/%m/%Y')
    except Exception:
        raise InvalidPayloadError('date_fin format is wrong')

    # Checking date_debut and date_fin year match each other
    if (
            date_debut.year != date_fin.year
    ):
        raise InvalidPayloadError('dates\' year should match')

    return {
        'id_meuble': payload.get('id_meuble'),
        'ad_libelle': payload.get('ad_libelle'),
        'ad_normalise': payload.get('ad_normalise'),
        'ad_latitude_normalise': payload.get('ad_latitude_normalise'),
        'ad_longitude_normalise': payload.get('ad_longitude_normalise'),
        'score': payload.get('score'),
        'ad_cp': payload.get('ad_cp'),
        'ad_ville': payload.get('ad_ville'),
        'ad_ban': payload.get('ad_ban'),
        'ad_insee': payload.get('ad_insee'),
        'ad_latitude': payload.get('ad_latitude'),
        'ad_longitude': payload.get('ad_longitude'),
        'ad_lot': payload.get('ad_lot'),
        'ad_bat': payload.get('ad_bat'),
        'ad_esc': payload.get('ad_esc'),
        'ad_etage': payload.get('ad_etage'),
        'ad_num_app': payload.get('ad_num_app'),
        'id_num': payload.get('id_num'),
        'est_residence_principale': payload.get('est_residence_principale'),
        'nb_nuits': payload.get('nb_nuits'),
        'date_debut': date_debut,
        'date_fin': date_fin,
        'url': payload.get('url'),
        'nom_loueur': payload.get('nom_loueur'),
        'created_by': token
    }


def post_batch_accomodations():
    """
    API v1 POST accomodations and related nights by batch (CSV input).

    CSV file should be semicolon-separated and have headers on first line. It
    should contain the following columns, in this order:
    * An internal id to uniquely identify a row,
    * The fulltext of the address,
    * The postal code of the city,
    * The full name of the city,
    * The BAN adresse format, if known,
    * The INSEE code of the city, if known,
    * The geolocation of the address in the "lat,long" format, if known,
    * The identifier of the accomodation, if known,
    * The building name or number, if known,
    * The stairs name or number, if known,
    * The floor number, if known,
    * The appartment or door number, if known,
    * The declaration number, empty if unknown,
    * Boolean matching True if the accomodation is a principal residence, otherwise False,
    * The total number of nights of the stay,
    * Starting date of the period.
    * Format is DD/MM/YYYY and it should match the starting date in the request for information from the city,
    * If specified, ending date of timeframe.
    * Format is DD/MM/YYYY and it should match the ending date in the request for information from the city.
    * The date's year must be the same as date_debut,
    * The unique and permanent URL of the accomodation, if known,
    * The name of the renter.


    :return: Nothing
    """
    # Handle CORS
    if bottle.request.method == 'OPTIONS':
        return {}

    # Check authentication
    try:
        token = check_auth('meubles')
    except AuthenticationError as e:
        return jsonapi.JsonApiError(403, "Invalid authentication. " + str(e))

    # Parse request
    try:
        payload = csv.DictReader(
            io.StringIO(bottle.request.body.read().decode('utf-8')),
            delimiter=';'
        )
    except ValueError:
        return jsonapi.JsonApiError(
            400,
            'Invalid CSV payload.'
        )

    # Log each and every query
    dump = PayloadDump(
        payload=bottle.request.body.read(),
        created_by=token
    )
    dump.save()

    status_code = 200
    status = {}
    # Create a list of bookings dict
    bookings = []

    # Checking payload shape
    # # Sometimes file can be sent or read as a UNIQUE line
    # It would result in a header list containing all data
    required_set = set([
        'id_meuble',
        'ad_libelle',
        'ad_cp', 'ad_ville',
        'est_residence_principale',
        'nb_nuits',
        'date_debut',
        'date_fin',
        'nom_loueur'])
    fields = required_set.union(set([
        'id_num',
        'ad_ban',
        'ad_insee',
        'ad_geo',
        'ad_lot',
        'ad_bat',
        'ad_esc',
        'ad_etage',
        'ad_num_app',
        'url']))
    try:
        payload_set = set(payload.fieldnames)
    except Exception:
        return jsonapi.JsonApiError(
            400,
            'Empty file'
        )
    if not required_set.issubset(payload_set):
        status_code = 400
        status['format'] = 'Missing required fields: ' + \
            str(required_set.difference(payload_set))
    if not payload_set.issubset(fields):
        status_code = 400
        status['format'] = 'Too many header fields: ' + \
            str(payload_set.difference(fields))
        status['format'] = status['format'] + \
            ' Make sure you are using ; as separator'

    size = 0
    for item in payload:
        size = size + 1
        booking_id = None
        try:
            # Mandatory checks raising specific errors
            if not item.get('id_meuble'):
                raise Exception('Can\'t read ID')
            booking_id = item.get('id_meuble')
            if not item.get('ad_libelle'):
                raise Exception('Empty adress')
            # When this field is sent, check for format validity
            if item.get('ad_geo'):
                if ',' in item.get('ad_geo'):
                    latitude, longitude = map(
                        float, item.get('ad_geo').split(','))
                else:
                    raise Exception('Wrong geolocation format')
            else:
                latitude, longitude = None, None
            bookings.append({
                'id_meuble': booking_id,
                'ad_libelle': item.get('ad_libelle'),
                'ad_cp': item.get('ad_cp'),
                'ad_ville': item.get('ad_ville'),
                'ad_ban': item.get('ad_ban'),
                'ad_insee': item.get('ad_insee'),
                'ad_latitude': latitude,
                'ad_longitude': longitude,
                'ad_lot': item.get('ad_lot'),
                'ad_bat': item.get('ad_bat'),
                'ad_esc': item.get('ad_esc'),
                'ad_etage': int(item.get('ad_etage')) if item.get('ad_etage') else None,
                'ad_num_app': int(item.get('ad_num_app')) if item.get('ad_num_app') else None,
                'id_num': item.get('id_num'),
                'est_residence_principale': strtobool(item.get('est_residence_principale')),
                'nom_loueur': item.get('nom_loueur'),
                'url': item.get('url'),
                'date_debut': item.get('date_debut'),
                'date_fin': item.get('date_fin'),
                'nb_nuits': int(item.get('nb_nuits')) if item.get('nb_nuits') else None
            })
        except Exception as e:
            status_code = 400
            status[booking_id] = 'NOK: Invalid payload. ' + str(e)

    if size == 0:
        status_code = 400
        status['format'] = 'No line in file' + status['format']

    with DATABASE_PROXY.atomic() as transaction:
        try:
            insert_many = []
            for booking in bookings:
                try:
                    booking_id = booking['id_meuble']
                    insert_many.append(
                        get_accomodation_dict(booking, token, fill_ban=False)
                    )

                    status[booking_id] = 'OK'
                except InvalidPayloadError as exc:
                    status_code = 400
                    status[booking_id] = 'NOK: %s' % str(exc)
                except Exception as e:
                    status_code = 400
                    status[booking_id] = 'NOK: Invalid payload. Error message : ' + str(e)
            if status_code == 400:
                raise
            for idx in range(0, len(insert_many), 1000):
                # Insert 1000 rows at a time.
                Meuble.insert_many(insert_many[idx:idx + 1000]).execute()
        # except peewee.ErrorSavingData:
        except Exception:
            transaction.rollback()

    return jsonapi.JsonApiError(
        status_code,
        {'status': status}
    )
