#!/usr/bin/env python
# coding: utf-8
#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Models and database definition
"""
import os

import peewee

from playhouse.db_url import connect

from server.tools import utc_now


DATABASE_PROXY = peewee.DatabaseProxy()


def init_proxy():
    """
    Return a peewee database object from a DB URI.
    """
    DATABASE_PROXY.initialize(
        connect(os.environ.get('DATABASE', 'postgresql:///api_meubles_test'))
    )


class BaseModel(peewee.Model):
    """
    Common base class for all models
    """
    class Meta:
        """
        Common meta-informations
        """
        database = DATABASE_PROXY


class APIKey(BaseModel):
    """
    An API key (token)
    """
    id = peewee.AutoField()
    token = peewee.CharField(max_length=255, index=True)
    scope = peewee.CharField(max_length=255)
    owner = peewee.CharField(max_length=255)
    expires_on = peewee.DateTimeField(
        default=utc_now, null=True
    )


class Meuble(BaseModel):
    """
    An accomodation and its rental duration per week

    Used to store raw input from platforms
    """
    id = peewee.AutoField()

    id_meuble = peewee.CharField(max_length=255, index=True, null=True)

    # == Address fields == #
    ad_libelle = peewee.CharField(
        max_length=255, null=True
    )
    ad_cp = peewee.CharField(
        max_length=5, null=True
    )
    ad_ville = peewee.CharField(max_length=255, null=True)

    ad_ban = peewee.CharField(
        max_length=255, index=True, null=True
    )
    ad_insee = peewee.CharField(max_length=5, null=True)
    ad_latitude = peewee.FloatField(null=True)
    ad_longitude = peewee.FloatField(null=True)
    ad_lot = peewee.CharField(max_length=255, null=True)
    ad_bat = peewee.CharField(max_length=255, null=True)
    ad_esc = peewee.CharField(max_length=255, null=True)
    ad_etage = peewee.IntegerField(null=True)
    ad_num_app = peewee.IntegerField(null=True)

    # == Metadata fields == #
    id_num = peewee.CharField(
        max_length=20, index=True, null=True
    )

    est_residence_principale = peewee.BooleanField(null=True)

    nom_loueur = peewee.CharField(
        max_length=255, null=True
    )
    url = peewee.CharField(
        max_length=255, null=True
    )

    # == Nights fields == #
    date_debut = peewee.DateField()
    date_fin = peewee.DateField()
    nb_nuits = peewee.IntegerField(
        constraints=[
            peewee.Check('nb_nuits >= 0'),
        ]
    )

    # == BAN API fields == #
    # These fields are used to enhance data quality and are extracted from
    # querying https://adresse.data.gouv.fr/api-doc/adresse
    ad_normalise = peewee.CharField(  # Adresse retournée par l'API de géocodage
        max_length=255, null=True
    )
    ad_latitude_normalise = peewee.FloatField(null=True)  # Coordonnées retournée par l'API
    ad_longitude_normalise = peewee.FloatField(null=True)  # Coordonnées retournée par l'API
    score = peewee.FloatField(null=True)  # Score de confiance des données associées

    # == Internal metadata fields == #
    created_on = peewee.DateTimeField(default=utc_now)
    created_by = peewee.CharField(max_length=255)


class PayloadDump(BaseModel):
    """
    Dump of an API payload (e.g. on errors)
    """
    id = peewee.AutoField()
    payload = peewee.TextField()
    created_on = peewee.DateTimeField(
        default=utc_now
    )
    created_by = peewee.CharField(max_length=255)
