#!/usr/bin/env python
# coding: utf-8
#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Main entry point, handle CLI
"""
import argparse
import os
import sys

from server import APP, generate_api_key
from server.models import APIKey
from server.tools import utc_now


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="cmd")
    subparsers.required = True
    run_parser = subparsers.add_parser(
        'run', help='Run web server'
    )
    generate_token_parser = subparsers.add_parser(
        'generate_token',
        help='Generate an API token and store it in the database.'
    )
    generate_token_parser.add_argument('scope')
    generate_token_parser.add_argument('owner')
    generate_token_parser.add_argument('expires', nargs='?')
    remove_expired_api_keys = subparsers.add_parser(
        'remove_expired_api_keys', help='Remove expired API keys.'
    )
    args = parser.parse_args()

    if args.cmd == 'run':
        APP.run(
            host=os.environ.get('HOST', '127.0.0.1'),
            port=int(os.environ.get('PORT', '3000'))
        )
    elif args.cmd == 'generate_token':
        # Could be used to handle different scopes
        # In order to deliver api keys to other parties
        SCOPES = ['meubles']
        if all(not args.scope.startswith(SCOPE) for SCOPE in SCOPES):
            sys.exit('Invalid scope, should be in %s.' % SCOPES)
        print(generate_api_key(args.scope, args.owner, args.expires))
    elif args.cmd == 'remove_expired_api_keys':
        query = APIKey.delete().where(APIKey.expires_on < utc_now())
        print('%s API keys removed.' % query.execute())
