#
# MIT License
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
"""
Handling authentication for the API
"""
import bottle
import peewee

from server.models import APIKey
from server.tools import utc_now


class AuthenticationError(Exception):
    """
    Exception raised on failed authentication.
    """


def check_auth(scope):
    """
    Check API authentication.

    :return: Abort and return a HTTP 403 page if authentication is not ok.
    """
    api_key = bottle.request.get_header('X-API-KEY')
    if not api_key:
        raise AuthenticationError
    # Check against whitelist
    try:
        key = APIKey.get(APIKey.token == api_key)
        if scope not in key.scope:
            raise AuthenticationError("Wrong scope")
        if key.expires_on and key.expires_on < utc_now():
            raise AuthenticationError("Key has expired")
        return api_key
    except peewee.DoesNotExist:
        raise AuthenticationError
